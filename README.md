# hid_mitm_pc

This is a program designed for use with [hid_mitm][hid_mitm]. It will send the inputs of any
connected gamepad to your Nintendo Switch, given its IP address.

## Usage

```sh
# use your switch's IP address
$ hid_mitm_pc 192.168.1.81
```

![screenshot](https://i.imgur.com/mhY6INO.png)
*Note*: For what it's worth, connecting the Pro Controller via Bluetooth and enabling it did
actually work, although you could just connect it to the Switch directly.

## Features

- Automatic reconnect on disconnect (to the Switch, not to the controller)
- Supports connecting and disconnecting controllers without restarting the program
- Multiple controllers (not support by hid_mitm, though, so currently useless)
- Pick and choose which controller(s) to use
- More accurate controls
  - Using the `input_pc_linux` binary from hid_mitm, actually using my DS4 in games was pretty
    difficult. Smash was very difficult to play, with R2 functioning as Start and ZR. hid_mitm_pc
    performs consistently better in my experience, and all the controls function as expected.
  - Also doesn't use the mouse as a controller
- Uses udev instead of sdl

## Download

- [Windows 64-bit](https://gitlab.com/jkcclemens/hid_mitm_pc/-/jobs/artifacts/master/raw/target/x86_64-pc-windows-gnu/release/hid_mitm_pc.exe?job=build_win_release)
- [Linux 64-bit](https://gitlab.com/jkcclemens/hid_mitm_pc/-/jobs/artifacts/master/raw/target/release/hid_mitm_pc?job=build_linux_release)

## Multiple controllers

hid_mitm_pc is designed to handle multiple controllers, as well and connects and disconnects. At the
time of writing this, however, hid_mitm does not support multiple controllers.

hid_mitm_pc assumes that multiple controller support will work by opening a separate socket for each
controller. If this is so, once multiple controller support is added, hid_mitm_pc will support it
automatically. If not, it will need to be updated.

Note that currently, if another controller is connected, that controller will supersede the previous
controller (both sockets will remain connected until one controller is disconnected). This will
probably cause strange or undesired behaviour.

[hid_mitm]: https://github.com/jakibaki/hid-mitm
