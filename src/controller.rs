use byteorder::{LittleEndian, WriteBytesExt};

use fxhash::{FxBuildHasher, FxHashMap};

use gilrs::{
  Gamepad,
  GamepadId,
  ev::{
    AxisOrBtn,
    Code,
    state::{
      AxisData,
      GamepadState,
    },
  },
};

use std::{
  collections::HashMap,
  io::Cursor,
  net::{
    SocketAddr,
    UdpSocket,
  },
};

use crate::Result;

pub struct Controller {
  // #[allow(dead_code)]
  pub id: GamepadId,
  pub codes: FxHashMap<AxisOrBtn, Code>,
  pub socket: UdpSocket,
}

impl Controller {
  pub fn new(gamepad: &Gamepad, addr: SocketAddr) -> Result<Self> {
    Ok(Controller {
      id: gamepad.id(),
      codes: Controller::codes(gamepad),
      socket: Controller::connect(addr)?,
    })
  }

  fn connect(addr: SocketAddr) -> Result<UdpSocket> {
    let socket = UdpSocket::bind("0.0.0.0:0")?;
    socket.connect(&addr).map_err(|e| failure::format_err!("could not connect: {}", e))?;

    Ok(socket)
  }

  pub fn reconnect(&mut self, addr: SocketAddr) -> Result<()> {
    self.socket = Controller::connect(addr)?;

    Ok(())
  }

  /// Gets the gamepad-specific codes with the abstracted button types necessary to send to the
  /// switch.
  fn codes(gamepad: &Gamepad) -> FxHashMap<AxisOrBtn, Code> {
    let mut codes = HashMap::with_capacity_and_hasher(
      crate::BUTTONS.len() + crate::AXES.len(),
      FxBuildHasher::default(),
    );

    for &button in crate::BUTTONS {
      if let Some(code) = gamepad.button_code(button) {
        codes.insert(AxisOrBtn::Btn(button), code);
      }
    }

    for &axis in crate::AXES {
      if let Some(code) = gamepad.axis_code(axis) {
        codes.insert(AxisOrBtn::Axis(axis), code);
      }
    }

    codes
  }

  /// Creates a u64 representing the state of the buttons in the given state.
  fn buttons(&self, state: &GamepadState) -> u64 {
    let mut u = 0;

    // as the buttons are listed in bit order, we can just loop through them and update the result
    for (i, &button) in crate::BUTTONS.iter().enumerate() {
      // only set the bit if the button is currently pressed
      if state.is_pressed(self.codes[&AxisOrBtn::Btn(button)]) {
        u |= 1 << i;
      }
    }

    u
  }

  /// Creates an array of 4 i32s representing the X and Y states of the two axes in the given state.
  fn axes(&self, state: &GamepadState) -> [i32; 4] {
    let mut axes = [0; 4];

    // take 4 just to prevent any mistakes
    for (i, &axis) in crate::AXES.iter().take(4).enumerate() {
      // get the code for this axis
      let code = self.codes[&AxisOrBtn::Axis(axis)];
      // assume 0 if no axis data for this axis
      let value = state.axis_data(code).map(AxisData::value).unwrap_or_default();
      // multiply the 0.0 to 1.0 value times the i16 max value
      let k = value * f32::from(std::i16::MAX);
      // cast the result to an i32 and set it (why this is serialised as an i32 is anyone's guess)
      axes[i] = k as i32;
    }

    axes
  }

  /// Broadcasts the given state to this controller's socket.
  pub fn broadcast(&self, state: &GamepadState) -> Result<()> {
    let mut cursor = Cursor::new(vec![0; 26]);
    cursor.write_u16::<LittleEndian>(crate::MAGIC)?;
    cursor.write_u64::<LittleEndian>(self.buttons(state))?;
    for &axis in &self.axes(state) {
      cursor.write_i32::<LittleEndian>(axis)?;
    }

    self.socket.send(cursor.get_ref())?;

    Ok(())
  }
}
