use crossbeam_channel::{Sender, Receiver, unbounded};

use fxhash::{FxBuildHasher, FxHashMap};

use gilrs::{GamepadId, Gilrs};

use cursive::{
  Cursive,
  align::HAlign,
  event::Key,
  menu::MenuTree,
  view::{Boxable, Identifiable, Scrollable, ScrollStrategy, SizeConstraint},
  views::{
    Checkbox,
    Dialog,
    DummyView,
    EditView,
    LinearLayout,
    ListView,
    OnEventView,
    Panel,
    TextView,
  },
};

use std::{
  collections::HashMap,
  net::{IpAddr, SocketAddr},
  sync::{
    Arc,
    atomic::{AtomicBool, Ordering},
  },
  thread::JoinHandle,
  str::FromStr,
};

use crate::{
  Result,
  controller::Controller,
  msg::{FromUiMsg, ToUiMsg},
};

pub struct State {
  pub gilrs: Gilrs,
  pub controllers: FxHashMap<GamepadId, Controller>,
  pub ui_tx: Option<Sender<ToUiMsg>>,
  pub addr: SocketAddr,
}

impl State {
  pub fn new(ip: IpAddr) -> Result<Self> {
    Ok(State {
      gilrs: Gilrs::new().map_err(|e| failure::format_err!("{}", e))?,
      controllers: HashMap::with_capacity_and_hasher(1, FxBuildHasher::default()),
      ui_tx: None,
      addr: SocketAddr::new(ip, 8080),
    })
  }

  pub fn set_addr(&mut self, ip: IpAddr) {
    self.addr = SocketAddr::new(ip, 8080);
  }

  fn ui_controllers(&self) -> Vec<(GamepadId, String, bool)> {
    self.gilrs.gamepads()
      .map(|(id, g)| (id, g.name().to_owned(), self.controllers.contains_key(&id)))
      .collect()
  }

  pub fn update_ui_controllers(&self) -> Result<()> {
    if let Some(tx) = self.ui_tx.as_ref() {
      tx.send(ToUiMsg::SetControllers(self.ui_controllers()))?;
    }
    Ok(())
  }

  pub fn ui_log<D: std::fmt::Display>(&self, d: D) -> Result<()> {
    if let Some(tx) = self.ui_tx.as_ref() {
      tx.send(ToUiMsg::add_log(d))?;
    }
    Ok(())
  }

  pub fn spawn_ui_thread(&mut self) -> (JoinHandle<()>, Sender<ToUiMsg>, Receiver<FromUiMsg>) {
    // create channels for ui messages
    let (to_ui_tx, to_ui_rx) = unbounded();
    let (from_ui_tx, from_ui_rx) = unbounded();

    // generate the initial list of gamepads
    let gamepads = self.ui_controllers();

    let quit_tx = from_ui_tx.clone();
    let thread_tx = from_ui_tx.clone();
    let switch_ip = self.addr.ip();

    let set_up_checkboxes = move |id, name, enabled, lv: &mut ListView| {
      let checkbox_tx = from_ui_tx.clone();
      let mut checkbox = Checkbox::new();
      checkbox.set_checked(enabled);
      checkbox.set_on_change(move |_cursive, checked| {
        checkbox_tx.send(FromUiMsg::SetController(id, checked)).ok();
      });
      let layout = LinearLayout::horizontal()
        .child(checkbox)
        .child(DummyView.fixed_width(1))
        .child(TextView::new(format!("{} ({})", name, id)));
      lv.add_child("", layout);
    };

    // spawn the ui thread
    let handle = std::thread::spawn(move || {
      let running = Arc::new(AtomicBool::new(true));

      // create the tui interface
      let mut cursive = Cursive::default();
      let quit_running = Arc::clone(&running);

      // create the controller list
      let mut list = ListView::new()
        .with_id("avail_controllers")
        .scrollable();
      // add the initial controller list
      for (id, name, enabled) in gamepads {
        set_up_checkboxes(id, name, enabled, &mut *list.get_inner_mut().get_mut());
      }

      let quit = move |_: &mut Cursive| {
        quit_tx.send(FromUiMsg::Quit).ok();
        quit_running.store(false, Ordering::Relaxed);
      };

      let layout = LinearLayout::vertical()
        .child(DummyView.fixed_height(1))
        .child(LinearLayout::horizontal()
          .child(TextView::new("Press q to quit. Press Esc for the menu."))
          .child(TextView::new(format!("Switch IP: {}", switch_ip))
            .h_align(HAlign::Right)
            .with_id("switch_ip")
            .boxed(SizeConstraint::Full, SizeConstraint::Free)))
        .child(DummyView.fixed_height(1))
        .child(Panel::new(list.boxed(SizeConstraint::Full, SizeConstraint::Full))
          .title("Available controllers"))
        .child(Panel::new(TextView::new("")
            .with_id("log")
            .scrollable()
            .scroll_strategy(ScrollStrategy::StickToBottom)
            .boxed(SizeConstraint::Full, SizeConstraint::Full))
          .title("Log"));
      let main_panel = Panel::new(layout)
        .title(env!("CARGO_PKG_NAME"))
        .full_screen();
      cursive.add_layer(
        OnEventView::new(main_panel)
          .on_event('q', quit.clone())
          .on_event(Key::Esc, Cursive::select_menubar)
      );

      let ip_tx = thread_tx.clone();
      let ip_change = move |s: &mut Cursive, ip_str: &str| {
        match IpAddr::from_str(&ip_str) {
          Ok(ip) => {
            ip_tx.send(FromUiMsg::SetIp(ip)).ok();
            s.call_on_id("switch_ip", |tv: &mut TextView| {
              tv.set_content(format!("Switch IP: {}", ip));
            });
            s.pop_layer();
          },
          Err(e) => s.add_layer(Dialog::around(TextView::new(format!("Bad IP: {}", e)))
            .title("Error")
            .button("Ok", |s| { s.pop_layer(); }))
        }
      };

      cursive.menubar()
        .add_subtree(
          "Switch",
          MenuTree::new().leaf("Change IP", move |s| {
            let ip_change = ip_change.clone();
            let inner_ip_change = ip_change.clone();

            let layout = LinearLayout::vertical()
              .child(TextView::new("Enter your Switch's IP address.\n\nAny connected controllers will need\nto be disabled and enabled after\nchanging."))
              .child(EditView::new()
                .on_submit(ip_change)
                .with_id("switch_ip_edit"));
            let dialog = Dialog::around(layout)
              .title("Change IP")
              .button("Save", move |s| {
                #[allow(clippy::redundant_closure)] // silence false positive
                let ip_str = s.call_on_id(
                  "switch_ip_edit",
                  |ev: &mut EditView| ev.get_content(),
                ).expect("missing switch_ip_edit");
                inner_ip_change(s, &ip_str)
              })
              .button("Cancel", |s| { s.pop_layer(); });
            s.add_layer(OnEventView::new(dialog)
              .on_event(Key::Esc, |s| { s.pop_layer(); }))
          }),
        )
        .add_leaf("Quit", quit);

      // do the initial ui refresh
      cursive.refresh();

      while running.load(Ordering::Relaxed) {
        // check for messages from the main loop
        while let Ok(msg) = to_ui_rx.try_recv() {
          match msg {
            // update the controller list
            ToUiMsg::SetControllers(cs) => {
              cursive.call_on_id("avail_controllers", |lv: &mut ListView| {
                // clear out the list
                lv.clear();
                // add each entry
                for (id, name, enabled) in cs {
                  set_up_checkboxes(id, name, enabled, lv);
                }
              });
            },
            ToUiMsg::AddLog(msg) => {
              cursive.call_on_id("log", |tv: &mut TextView| {
                tv.append(format!("{}\n", msg));
              });
            },
          }
          // refresh the ui after processing a message
          cursive.refresh();
        }

        // keep the ui running
        cursive.step();
      }
    });

    self.ui_tx = Some(to_ui_tx.clone());

    (handle, to_ui_tx, from_ui_rx)
  }
}
