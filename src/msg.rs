use gilrs::GamepadId;

use std::net::IpAddr;

#[derive(Debug)]
pub enum ToUiMsg {
  /// Set the list of controllers to the list given.
  SetControllers(Vec<(GamepadId, String, bool)>),
  AddLog(String),
}

impl ToUiMsg {
  pub fn add_log<D: std::fmt::Display>(d: D) -> Self {
    ToUiMsg::AddLog(format!("{}", d))
  }
}

#[derive(Debug)]
pub enum FromUiMsg {
  /// Toggle the given controller off or on.
  SetController(GamepadId, bool),
  /// Stop the main loop and exit.
  Quit,
  /// Set the Switch's IP address.
  SetIp(IpAddr),
}
