use gilrs::{
  Axis,
  Button,
  Event,
  ev::EventType,
};

use std::{
  net::IpAddr,
  str::FromStr,
  time::Duration,
};

mod controller;
mod msg;
mod state;

use crate::{
  state::State,
  msg::FromUiMsg,
  controller::Controller,
};

pub type Result<T> = std::result::Result<T, failure::Error>;

fn main() -> Result<()> {
  // take the ip of the switch as the first arg
  let ip_str = match std::env::args().nth(1) {
    Some(s) => s,
    None => {
      eprintln!("please specify the switch ip addr (v4/v6)");
      return Ok(());
    },
  };
  let ip = IpAddr::from_str(&ip_str)?;

  let mut state = State::new(ip)?;
  let (ui_handle, _, from_ui_rx) = state.spawn_ui_thread();

  'main: loop {
    // we always have to loop through the events to update gilrs' state
    while let Some(Event { id, event, .. }) = state.gilrs.next_event() {
      match event {
        // if a controller disconnects, remove it from the controller map. this will close the
        // socket
        EventType::Disconnected => {
          state.controllers.remove(&id);
          state.update_ui_controllers()?;
        },
        // update the ui if a controller connects
        EventType::Connected => state.update_ui_controllers()?,
        _ => {},
      }
    }

    // check for messages from the ui
    while let Ok(msg) = from_ui_rx.try_recv() {
      #[allow(clippy::map_entry)] // ignore false positive
      match msg {
        // remove or add a controller to the enabled list if the ui says to
        FromUiMsg::SetController(id, checked) => if !checked {
          state.controllers.remove(&id);
          let g = state.gilrs.gamepad(id);
          state.ui_log(format_args!("Disabled {} ({})", g.name(), id))?;
        } else if let Some(gamepad) = state.gilrs.connected_gamepad(id) {
          match Controller::new(&gamepad, state.addr) {
            Ok(c) => {
              state.controllers.insert(id, c);
              state.ui_log(format_args!("Enabled {} ({})", gamepad.name(), id))?;
            },
            Err(e) => state.ui_log(format_args!("Could not enable {} ({}): {}", gamepad.name(), id, e))?,
          }
        },
        // change the switch's ip
        // TODO: reconnect controllers
        FromUiMsg::SetIp(ip) => state.set_addr(ip),
        // quit the program
        FromUiMsg::Quit => break 'main,
      }
      // update the ui after processing a message from it
      state.update_ui_controllers()?;
    }

    // run through each connected gamepad
    let mut remove = Vec::default();
    for (&id, controller) in &state.controllers {
      // send the state of the gamepad to the switch
      if let Err(e) = controller.broadcast(state.gilrs.gamepad(id).state()) {
        state.ui_log(format_args!("Could not broadcast controller input: {}", e))?;
        // queue the controller to be removed if broadcast failed
        remove.push(id);
      }
    }
    let remove_any = !remove.is_empty();
    // remove controllers that failed to broadcast if they can't reconnect
    for id in remove {
      let c = state.controllers.get_mut(&id).expect("controller missing");
      // both of these turn out to be false
      // if dbg!(c.reconnect(state.addr).is_err()) || dbg!(c.broadcast(state.gilrs.gamepad(id).state()).is_err()) {
      if c.reconnect(state.addr).is_err() {
        state.ui_log("Could not reconnect controller")?;
        state.controllers.remove(&id);
      }
    }
    // update the ui after removing any controller
    if remove_any {
      state.update_ui_controllers()?;
    }

    // sleep for 16ms (60fps)
    std::thread::sleep(Duration::from_millis(16));
  }

  ui_handle.join().ok();

  Ok(())
}

/// A list of buttons that will be sent to the switch, in order.
const BUTTONS: &[Button] = &[
  Button::East,
  Button::South,
  Button::North,
  Button::West,
  Button::LeftThumb,
  Button::RightThumb,
  Button::LeftTrigger,
  Button::RightTrigger,
  Button::LeftTrigger2,
  Button::RightTrigger2,
  Button::Start,
  Button::Select,
  Button::DPadLeft,
  Button::DPadUp,
  Button::DPadRight,
  Button::DPadDown,
];

/// A list of axes that will be sent to the switch, in order.
const AXES: &[Axis] = &[
  Axis::LeftStickX,
  Axis::LeftStickY,
  Axis::RightStickX,
  Axis::RightStickY,
];

/// The magic bytes sent before each packet.
const MAGIC: u16 = 0x3275;
